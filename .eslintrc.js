module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: 'standard',
  overrides: [
    {
      env: {
        node: true
      },
      files: ['.eslintrc.{js,cjs}'],
      parserOptions: {
        sourceType: 'script'
      }
    },
    {
      files: ['webpack.config.js'],
      rules: {
        'no-inline-comments': 'off'
      }
    },
    {
      files: ['*.js'], // Apply the rules only to JavaScript files
      env: {
        node: true // Set the environment to Node.js for JavaScript files
      },
      rules: {
        'no-console': 'off',
        'global-require': 'off',
        'no-unused-expressions': 'off',
        quotes: ['error', 'single'],
        camelcase: ['error', { properties: 'always' }],
        'key-spacing': 'error',
        'keyword-spacing': 'error',
        'linebreak-style': 'error',
        'lines-around-comment': 'error',
        'max-depth': 'error',
        'max-len': 'error'
      }
    }
    // {
    //   files: ['*.html'],
    //   rules: {
    //     indent: ['error', 2]
    //   }
    // }
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  rules: {
    quotes: ['error', 'double'],
    indent: ['error', 2],
    'linebreak-style': ['error', 'unix'],
    semi: ['error', 'always'],
    camelcase: ['error', { properties: 'always' }],
    'no-console': 'off',
    'prefer-const': 'error',
    'no-dupe-keys': 'error',
    'prefer-spread': 'error',
    'arrow-spacing': 'error',
    'no-unused-vars': 'off',
    'no-else-return': 'warn',
    'no-underscore-dangle': ['error', { allow: ['_privateVariable'] }],
    'object-curly-spacing': ['error', 'always'],
    'no-extra-parens': ['error', 'all', { nestedBinaryExpressions: false }]
  }
};
