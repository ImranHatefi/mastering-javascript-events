/* eslint-disable */
const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

export function validate(email) {
  const emailEnding = email.split('@')[1]; // Extract the ending after '@'
  return VALID_EMAIL_ENDINGS.includes(emailEnding)
}

// export function validateAsync(email) {
//   return new Promise(resolve => {
//     process.nextTick(() => {
//       const isValid = validate(email);
//       resolve(isValid);
//     });
//   });
// }

export  function validateAsync(email) {
	new Promise(resolve => process.nextTick(resolve)); // Simulate asynchronous behavior
  
	const isValid = validate(email);
	return isValid;
  }
  
export function validateWithThrow(email) {
	if (!validate(email)) {
	  throw new Error("Invalid email");
	}
	return true;
}
  
export function validateWithLog(email) {
	const isValid = validate(email);
	console.log(`Email "${email}" validation result: ${isValid}`);
	return isValid;
}
