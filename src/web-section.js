/* eslint-disable*/
/* eslint-disable*/

// Define the class
class WebsiteSection extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.render();
  }

  render() {
    const isImgSection = this.getAttribute("data-status") === "img-section";
    const isLeft = this.getAttribute("data-left") === "left";
    const isRight = this.getAttribute("data-right") === "right";
    const isHeart = this.getAttribute("data-heart") === "true";
    const isTrueStatus = this.getAttribute("data-status") === "true";
    

    this.shadowRoot.innerHTML = `
    <style>
    :host {
      display: block;
    }
    .app-title {
      font-size: 3.25rem;
      line-height: 3.2rem;
      text-align: center;
    }
    .app-subtitle {
      font-size: 1.5rem;
      line-height: 1.625rem;
      font-weight: 300;
      text-align: center;
      font-family: "Source Sans Pro", sans-serif, Arial;
    }
    .app-section_jointeam {
      display: flex;
      flex-direction: row;
      justify-content: space-around;
      width: 1200px;
      height: 515px;
      flex-shrink: 0;
      background: #eee;
    }
    
    .app-section_jointeam--img {
      width: 334px;
      height: 334px;
      flex-shrink: 0;
      background: lightgray 50% / cover no-repeat;
      border-radius: 50%;
    }
    
    .join-team-title {
      width: 280px;
      height: 125px;
      flex-shrink: 0;
      color: #464547;
      font-family: Oswald;
      font-size: 38px;
      font-style: normal;
      font-weight: 700;
      line-height: 64px;
    }
    .join-team-title2 {
      width: 480px;
      height: 120px;
      flex-shrink: 0;
      color: #464547;
      font-family: Oswald;
      font-size: 48px;
      font-style: normal;
      font-weight: 700;
      line-height: 64px;
    
    }
    
    .join-team-info {
      width: 441px;
      height: 127px;
      flex-shrink: 0;
      color: #666;
      font-family: Source Sans Pro;
      font-size: 24px;
      font-style: normal;
      font-weight: 400;
      line-height: 32px;
      /* 133.333% */
    }
    h1{
      margin:0
    }
    
    .join-team-btn {
      width: 144px;
      height: 36px;
      flex-shrink: 0;
      border-radius: 18px;
      background: #55c2d8;
      color: #fff;
      text-align: center;
      font-family: Oswald;
      font-size: 14px;
      font-style: normal;
      font-weight: 400;
      line-height: 26px;
      /* 185.714% */
      letter-spacing: 1.4px;
      border: none;
      cursor: pointer;
    }
    
    .btn-join {
      margin: auto;
      display: flex;
      justify-content: center;
    }
    .btn-join > .order2 {
      order: 1;
    }
    
    .app-section_wps {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      width: 1200px;
      height: 531px;
      flex-shrink: 0;
      background: #1a9cb0;
    }
    
    .app-section_wps_head {
      display: flex;
      flex-direction: row;
      gap: 312px;
      margin-bottom: 60px;
      margin-top: -150px;
    }
    
    .app-section_wps_head--header {
      margin-left: 340px;
      width: 151px;
      height: 14px;
      flex-shrink: 0;
      color: #fff;
      font-family: Source Sans Pro;
      font-size: 16px;
      font-style: normal;
      font-weight: 400;
      line-height: 18px;
      /* 112.5% */
      letter-spacing: 1.6px;
    }
    
    .app-section_wps_head--see {
      width: 47px;
      height: 14px;
      flex-shrink: 0;
      color: #fff;
      font-family: Source Sans Pro;
      font-size: 12px;
      font-style: normal;
      font-weight: 400;
      line-height: 18px;
      /* 150% */
      letter-spacing: 1.2px;
      text-decoration-line: underline;
    }
    @media only screen and (max-width: 768px) {
      .join-team-title {
        width: 100%;
        text-align: center;
      }
      .join-team-info {
        width: 95%;
        text-align: center;
      }
      .join-team-btn {
        display: flex;
        justify-content: center;
        align-content: center;
        align-items: center;
        margin: auto;
        margin-top: 20px;
      },
    }
  </style>

${isImgSection
          ? `
					${isLeft ? "<slot></slot>" : ""}
					<div>
						<h1 class="join-team-title">${this.getAttribute("title")}</h1>
						<h2 class="join-team-info">${this.getAttribute("description")}</h2>
						<div class="btn-join">
							<button class="join-team-btn">JOIN OUR TEAM</button>
						</div>
					</div>
					${isRight ? "<slot></slot>" : ""}
				`
          : `
					${isHeart ? "<slot></slot>" : ""}
					<h1 class="app-title">${this.getAttribute("title")}</h1>
					${isTrueStatus ? "<slot></slot>" : ""}
					<h2 class="app-subtitle">${this.getAttribute("description")}</h2>
					<slot></slot>
				`
      }
`;
  }
}

customElements.define("website-section", WebsiteSection);

// class WebsiteSection extends HTMLElement {
//   constructor () {
//     super();

//     const template = document.createElement("template");
//     template.innerHTML = `
//     <style>
//     .app-title {
//         font-size: 3.25rem;
//         line-height: 3.2rem;
//         text-align: center;
//       }

//       .app-subtitle {
//         font-size: 1.5rem;
//         line-height: 1.625rem;
//         font-weight: 300;
//         text-align: center;
//         font-family: "Source Sans Pro", sans-serif, Arial;
//       }
//   </style>

//         <h1 class="app-title">${this.getAttribute("title")}</h1>
//         <h2 class="app-subtitle">${this.getAttribute("description")}</h2>
//         <slot></slot>
//        `;

//     this.attachShadow({ mode: "open" });
//     this.shadowRoot.appendChild(template.content.cloneNode(true));
//   }
// }

// customElements.define('web-section', WebsiteSection);



// const isWPS = this.getAttribute("data-wps") === "isWPS";

// ${isWPS && !isImgSection  && !isHeart && !isT
//   ? `
//         <div class='app-section_wps'>
//             <div class='app-section_wps_head'>
//                 <h3 class="app-section_wps_head--header">${this.getAttribute(
//                   "title"
//                 )}</h3>
//                 <span class="app-section_wps_head--see">${this.getAttribute(
//                   "description"
//                 )}</span>
//             </div>
//         </div>
//     `
//   : ``
// }