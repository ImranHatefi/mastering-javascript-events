/* eslint-disable */
const BATCH_SIZE = 5;
const clickDataBatch = [];

self.addEventListener("message", event => {
  const { type, data } = event.data;
  if (type === "click") {
    
    const clickEvent = {
      timestamp: data.timestamp,
      element: data.element,
    };

    clickDataBatch.push(clickEvent);

    if (clickDataBatch.length >= BATCH_SIZE) {
      sendBatchToServer(clickDataBatch);
      clickDataBatch.length = 0;
    }
  }
});

function sendBatchToServer(batch) {
  fetch("http://localhost:3000/analytics/user", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(batch),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
      }
      return response.json();
    })
    .then((result) => {
      if (result.success) {
        console.log("Click data batch sent successfully");
      } 
    })
    .catch((error) => {
      console.error("Error sending click data batch:", error);
    });
}
